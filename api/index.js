// const bull = require("bull");
// const {redis} = require("../src/settings");
const {queueView, queueCreate, queueDelete, queueFindOne, queueUpdate} 
        = require("../src/Adapters/index");

async function Create() {
  try {
    const job = queueCreate.add({ age, color, name });
    const result = await job.finished();
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}
async function Delete() {
  try {
    const job = queueCreate.add({is});
    const result = await job.finished();
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}
async function FindOne() {
  try {
    const job = queueFindOne.add({name});
    const result = await job.finished();
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}
async function Update() {
  try {
    const job = queueUpdate.add({age, color, id, name});
    const result = await job.finished();
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}
async function View() {
  try {
    const job = queueView.add({});
    const result = await job.finished();
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}

async function main(){
    View()
}

main();
