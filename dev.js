const micro = require('./src')

async function main(){
    try{
        const db= await micro.SyncDB();
        if(db.statusCode !== 200) throw db.message
        await micro.run(); //si no hay problemas corremos el micro

    }catch(error){
        console.log(error)
    }
}
main() //pausa min 15 - video 5