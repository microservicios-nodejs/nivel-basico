const {sequelize} = require("../settings");
const { DataTypes } = require("sequelize");

const Model = sequelize.define("curso", {
  name: { type: DataTypes.STRING },
  age: { type: DataTypes.BIGINT },
  color:{ type: DataTypes.STRING },
});

async function SyncDB(){ //adaptado al estandar que utilizamos
    try{
        console.log("Inicializando base de datos")
        await Model.sync()
        return {statusCode: 200, data: "ok"} 
        console.log("BD inicializada con exito");
    }catch(error){
      console.log(error)
      return { statusCode: 500, message: error.toString() };
    }
}

module.exports = { SyncDB, Model };
