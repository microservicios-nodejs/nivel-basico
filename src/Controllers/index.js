const { Model } = require("../Models");

async function Create({ name, age, color }) {
  try {
    const instance = await Model.create(
      {name, age, color}, {fields: ["name", "age", "color"]}
    );
    return { statusCode: 200, data: instance.toJSON() };
  } 
  catch (error) {
       console.log({step: "controller Create", error: error.toString(),});

    return { statusCode: 400, message: error.toString() };
  }
}

async function Delete({ where = {} }) {
  try {
    await Model.destroy({ where });
    return { statusCode: 200, data: "OK" };
  } 
  catch (error) {
    console.log({step: "controller Create",error: error.toString()});
    return { statusCode: 400, message: error.toString() };
  }
}

async function Update({ name, age, color, id }) {
  try {
    const instance = await Model.create(
      { name, age, color,},{ where: { id }, }
    );
    return { statusCode: 200, data: instance[1][0].toJSON() };
  } 
  catch (error) {
    console.log({
       step: "controller Create",
       error: error.toString(),
    });
    return { statusCode: 400, message: error.toString() };
  }
}

async function FindOne({ where = {} }) {
  try {
    const instance = await Model.findOne({ where });

    return { statusCode: 200, data: instance.toJSON() };
  } 
  catch (error) {
    console.log({step: "controller Create",error: error.toString(),});
    return { statusCode: 400, message: error.toString() };
  }
}

async function View({ where = {} }) {
  try {
    const instances = await Model.findAll({ where });

    return { statusCode: 200, data: instances };
  } 
  catch (error) {
    console.log({ step: "controller Create", error: error.toString(), });
    return { statusCode: 200, message: error.toString() };
  }
}
module.exports = { 
  Create, 
  Delete, 
  Update, 
  FindOne, 
  View };
