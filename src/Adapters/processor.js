const Services = require("../Services");
const { InternalError } = require("../settings");
const { queueView, queueCreate, queueDelete, 
      queueUpdate, queueFindOne } = require("./index");

//callBack
// (job, done)=>{View(job, done)}   ==  View

//Servicio vista
async function View (job, done) {
  const { } = job.data;
  try {
    const { statusCode, data, message } = await Services.View({});
    done(null, { statusCode, data, message });
  } 
  catch (error) {
    console.log({ step: "adapters queueView",error: error.toString(), });
    done(null, { statusCode: 500, message: InternalError });
  }
};

//Servicio Crear
async function Create(job, done) {
  const { age, color, name } = job.data;
  try {
    const { statusCode, data, message } = await Services.Create({ name, age,color, id,});
    done(null, { statusCode, data, message });
  } 
  catch (error) {
    console.log({ step: "adapters queueCreate", error: error.toString(),
    });
    done(null, { statusCode: 500, message: InternalError });
  }
};
//Servicio eliminar
async function Delete(job, done) {
  const { id } = job.data;
  try {
    const { statusCode, data, message } = await Services.Delete({ id });
    done(null, { statusCode, data, message });
  } 
  catch (error) {console.log({step: "adapters queueView", error: error.toString(),});
    done(null, { statusCode: 500, message: InternalError });
  }
};

//Servicio busqueda por id
async function FindOne(job, done) {
  const { age, color, id, name } = job.data;
  try {
    const { statusCode, data, message } = await Services.FindOne({age,color, id,name});
    done(null, { statusCode, data, message });
  } 
  catch (error) {
    console.log({ step: "adapters queueView", error: error.toString(),});
    done(null, { statusCode: 500, message: InternalError });
  }
};

//Servicio actualizar
async function Update(job, done) {

  const { age, color, id, name } = job.data;
  try {
    const { statusCode, data, message } = await Services.Update({age,color, id,name});
    done(null, { statusCode, data, message });
  } 
  catch (error) {
    console.log({step: "adapters queueView", error: error.toString() });
    done(null, { statusCode: 500, message: InternalError });
  }
};

async function run(){
  try{
    console.log("Inicializando workwer-run");
    queueView.process(View)
    queueUpdate.process(Update);
    queueFindOne.process(FindOne);
    queueCreate.process(Create);
    queueDelete.process(Delete);

  }catch(error){
      console.log(error)
  }
}

module.exports = { View, Update, FindOne, Create, Delete,//correr de forma especilizada
                 run }; //corremos todo el micro
