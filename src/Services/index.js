//Importamos Controllers
const Controllers = require("../Controllers");
//{ Create, Delete, Update, FindOne, View }

//CREAR
async function Create({ age, color, name }) {
try {
  let { statusCode, data, message } = await Controllers.Create({age,color, name,});
  return { statusCode, data, message };

} 
catch (error) {
  console.log({ step: "service Create", error: error.toString() });
  return { statusCode: 500, message: error.toString() };
}
}
//ELIMINAR
async function Delete({ id }) {
	try {
		let { statusCode, data, message } =await Controllers.Delete({where: { id } });
		return { statusCode, data, message };
	} 
catch (error) {
  console.log({ step: "service Delete", error: error.toString() });
  return { statusCode: 500, message: error.toString() };
}
}
//ACTUALIZAR
async function Update({ name, age, color, id }) {
	try {
		let { statusCode, data, message } = await Controllers.Update({ name, age,  color, id });
		return { statusCode, data, message };
	} 
  catch (error) {
  console.log({ step: "service Update", error: error.toString() });
  return { statusCode: 500, message: error.toString() };
}
}
//BUSCAR POR ID
async function FindOne({ name, age, color, id }) {
	try {
		let { statusCode, data, message } = await Controllers.FindOne({
			where: {	name,age,	color,	id}	});
		return { statusCode, data, message };
	} 
  catch (error) {
  console.log({ step: "service FindOne", error: error.toString() });
  return { statusCode: 500, message: error.toString() };
}
}

//VER TDO
async function View({ name, age, color, id }) {
	try {
		let { statusCode, data, message } = await Controllers.View({
			where: {	name,	age,	color,	id}	});
		return { statusCode, data, message };
	}
  catch (error) {
  console.log({ step: "service View", error: error.toString() });
  return { statusCode: 500, message: error.toString() };
}
}

module.exports = {
  	Create,
	  Delete,
	  Update,
	  FindOne,
	  View
 };
